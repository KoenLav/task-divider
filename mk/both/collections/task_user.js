this.TaskUser = new Mongo.Collection("task_user");

this.TaskUser.userCanInsert = function(userId, doc) {
	return true;
}

this.TaskUser.userCanUpdate = function(userId, doc) {
	return true;
}

this.TaskUser.userCanRemove = function(userId, doc) {
	return true;
}
