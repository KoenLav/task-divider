SyncedCron.add({
    name: 'Email task reminders',
    schedule: function(parser) {
        // Recur every day at 15:00
        return parser.recur().on(15).hour()
    },
    job: function() {
        var find = {
            'date': {
                $gt: moment().startOf('day').toDate(),                  // Today
                $lte: moment().startOf('day').add(1, 'days').toDate()   // Tomorrow
            },
            'completed': false
        }

        // Create a function which returns a variable depending on the assignment
        var text = function(assignment) {
            return 'Hoi ' + assignment.user.profile.name + ',' +
            '\n\n' +
            'Uit onze administratie blijkt dat je de taak ' + assignment.task.name + ' nog niet voltooid hebt.' +
            '\n\n' +
            'Met vriendelijke groet,' +
            '\n\n' +
            'Your friendly neighborhood schoonmaak robo-nazi'
        }

        sendTaskUserEmail(find, text)
    }
});

function sendTaskUserEmail(find, text) {
    TaskUser.find(find).fetch().forEach(function(assignment) {
        Email.send({
            from: 'Schoonmaakrooster <no-reply@getright.nl>',
            to: assignment.user.profile.email,
            subject: 'Schoonmaaktaak DS95',
            text: text(assignment)
        })
    })
}

SyncedCron.start()