TaskUser.allow({
	insert: function (userId, doc) {
		return TaskUser.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return TaskUser.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return TaskUser.userCanRemove(userId, doc);
	}
});

TaskUser.before.insert(function(userId, doc) {
	doc.createdAt = new Date();
	doc.createdBy = userId;
	doc.modifiedAt = doc.createdAt;
	doc.modifiedBy = doc.createdBy;

	
	if(!doc.createdBy) doc.createdBy = userId;
});

TaskUser.before.update(function(userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	
});

TaskUser.before.remove(function(userId, doc) {
	
});

TaskUser.after.insert(function(userId, doc) {
	
});

TaskUser.after.update(function(userId, doc, fieldNames, modifier, options) {
	
});

TaskUser.after.remove(function(userId, doc) {
	
});
