Meteor.publish("task_user", function() {
	var n = 0;

	Tasks.find().fetch().forEach(function(task) {
		n += task.people_required
	})

	return TaskUser.find({},
		{
			sort: {
				'date': -1
			},
			limit: n * 10
		}
	);
});

Meteor.publish("task_user_empty", function() {
	return TaskUser.find({}, {});
});

