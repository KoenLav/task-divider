Meteor.publish("tasks", function() {
	return Tasks.find({}, {});
});

Meteor.publish("task_empty", function() {
	return Tasks.find({}, {});
});

Meteor.publish("admin_task", function(taskId) {
	return Tasks.find({_id:taskId}, {});
});

