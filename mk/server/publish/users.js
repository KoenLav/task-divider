Meteor.publish("users", function() {
    return Users.find({}, {
        fields: {
            profile: 1
        }
    });
});