var pageSession = new ReactiveDict();

Template.HomePrivateView.onCreated(function() {
    var recurringPeriod = {
            start: 8,
            every: 14,
            unit: 'day'
        },
        planPeriodsInAdvance = 3,
        startDate = moment().day(recurringPeriod.start).startOf('day').toDate(),
        prev = TaskUser.findOne({}, {sort:{'date':'desc'}}),
        nextDate

    if (typeof prev == 'undefined' || prev.date <= moment().add(planPeriodsInAdvance * recurringPeriod.every, recurringPeriod.unit).toDate()) {
        if (typeof prev != 'undefined') {
            nextDate = moment(prev.date).add(recurringPeriod.every, recurringPeriod.unit).startOf('day').toDate()
        }
        else {
            nextDate = startDate
        }

        var users = Users.find().fetch().shuffle()

        var i = 0

        // Get all tasks, shuffle them and assign each task to n persons (n defined by people_required)
        Tasks.find().fetch().shuffle().forEach(function (task) {
            for (var j = 0; j < task.people_required; j++) {
                // We assigned every roommate a task: reshuffle the array and reset i to zero
                if (typeof users[i] == 'undefined') {
                    users.shuffle()

                    i = 0
                }

                TaskUser.insert({
                    date: nextDate,
                    user: users[i],
                    task: task,
                    completed: false,
                    confirmed: false
                })

                i++
            }
        })
    }
});

Template.currentAssignmentTable.viewmodel({
    assignments: function() {
        var n = 0;

        Tasks.find().fetch().forEach(function(task) {
            n += task.people_required
        })

        return TaskUser.find({
            date: {
                $gte: new Date()
            }},
            {
                sort: {
                    'date': 1,
                    'task.name': 1,
                    'user.profile.name': 1
                },
                limit: n
            })
    }
})

Template.previousAssignmentTable.viewmodel({
    assignments: function() {
        return TaskUser.find({
                date: {
                    $lt: new Date()
                }},
            {
                sort: {
                    'date': 1,
                    'task.name': 1,
                    'user.profile.name': 1
                }
            })
    }
})

Template.assignment.viewmodel({
    assignment: function() {
        return TaskUser.findOne(this._id())
    },

    isCompleted: function() {
        return this.assignment().completed != false
    },

    isConfirmed: function() {
        return this.assignment().confirmed != false
    },

    setCompleted: function(e) {
        e.preventDefault()

        if (Meteor.userId() == this.assignment().user._id) {
            if (this.isCompleted()) {
                TaskUser.update({_id: this._id()}, {$set: {'completed': false, 'confirmed': false}})
            }
            else {
                TaskUser.update({_id: this._id()}, {$set: {'completed': moment().toDate()}})
            }

            return true;
        }

        bootbox.dialog({
            message: "Alleen eigen taken kunnen worden afgerond.",
            title: "Afronden niet mogelijk",
            animate: true,
            buttons: {
                success: {
                    label: "OK",
                    className: "btn-default"
                }
            }
        })

        return false
    },

    setConfirmed: function(e) {
        e.preventDefault()

        console.log(Meteor.userId() != this.assignment().user._id && this.isCompleted())
        if (Meteor.userId() != this.assignment().user._id && this.isCompleted()) {
            if (this.isConfirmed()) {
                TaskUser.update({_id: this._id()}, {$set: {'confirmed': false}})
            }
            else {
                TaskUser.update({_id: this._id()}, {$set: {'confirmed': Meteor.userId()}})
            }

            return true
        }

        bootbox.dialog({
            message: "Taken die niet afgerond zijn en eigen taken kunnen niet bevestigd worden.",
            title: "Bevestigen niet mogelijk",
            animate: true,
            buttons: {
                success: {
                    label: "OK",
                    className: "btn-default"
                }
            }
        })

        return false
    }
})

Template.HomePrivateView.helpers({
    "isEmpty": function() {
        return !this.task_user || this.task_user.count() == 0;
    },
    "isNotEmpty": function() {
        return this.task_user && this.task_user.count() > 0;
    }
});
