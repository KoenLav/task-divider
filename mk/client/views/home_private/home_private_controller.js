this.HomePrivateController = RouteController.extend({
	template: "HomePrivate",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		var subs = [
			Meteor.subscribe("users"),
			Meteor.subscribe("tasks"),
			Meteor.subscribe("task_user")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		return {
			params: this.params || {},
			task_user: TaskUser.find({}, {})
		};
		/*DATA_FUNCTION*/
	},

	onAfterAction: function() {

	}
});