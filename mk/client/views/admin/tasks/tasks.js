var pageSession = new ReactiveDict();

Template.AdminTasks.rendered = function() {
	
};

Template.AdminTasks.events({
	
});

Template.AdminTasks.helpers({
	
});

var AdminTasksViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("AdminTasksViewSearchString");
	var sortBy = pageSession.get("AdminTasksViewSortBy");
	var sortAscending = pageSession.get("AdminTasksViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["name", "description", "people_required"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var AdminTasksViewExport = function(cursor, fileType) {
	var data = AdminTasksViewItems(cursor);
	var exportFields = [];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.AdminTasksView.rendered = function() {
	pageSession.set("AdminTasksViewStyle", "table");
	
};

Template.AdminTasksView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("AdminTasksViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("AdminTasksViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("AdminTasksViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("admin.tasks.insert", {});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		AdminTasksViewExport(this.tasks, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		AdminTasksViewExport(this.tasks, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		AdminTasksViewExport(this.tasks, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		AdminTasksViewExport(this.tasks, "json");
	}

	
});

Template.AdminTasksView.helpers({

	"insertButtonClass": function() {
		return Tasks.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.tasks || this.tasks.count() == 0;
	},
	"isNotEmpty": function() {
		return this.tasks && this.tasks.count() > 0;
	},
	"isNotFound": function() {
		return this.tasks && pageSession.get("AdminTasksViewSearchString") && AdminTasksViewItems(this.tasks).length == 0;
	},
	"searchString": function() {
		return pageSession.get("AdminTasksViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("AdminTasksViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("AdminTasksViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("AdminTasksViewStyle") == "gallery";
	}

	
});


Template.AdminTasksViewTable.rendered = function() {
	
};

Template.AdminTasksViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("AdminTasksViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("AdminTasksViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("AdminTasksViewSortAscending") || false;
			pageSession.set("AdminTasksViewSortAscending", !sortAscending);
		} else {
			pageSession.set("AdminTasksViewSortAscending", true);
		}
	}
});

Template.AdminTasksViewTable.helpers({
	"tableItems": function() {
		return AdminTasksViewItems(this.tasks);
	}
});


Template.AdminTasksViewTableItems.rendered = function() {
	
};

Template.AdminTasksViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		Router.go("admin.tasks.details", {taskId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		Tasks.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						Tasks.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("admin.tasks.edit", {taskId: this._id});
		return false;
	}
});

Template.AdminTasksViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return Tasks.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return Tasks.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
