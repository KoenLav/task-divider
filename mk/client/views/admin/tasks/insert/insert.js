var pageSession = new ReactiveDict();

Template.AdminTasksInsert.rendered = function() {
	
};

Template.AdminTasksInsert.events({
	
});

Template.AdminTasksInsert.helpers({
	
});

Template.AdminTasksInsertInsertTask.rendered = function() {
	

	pageSession.set("adminTasksInsertInsertTaskInfoMessage", "");
	pageSession.set("adminTasksInsertInsertTaskErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.AdminTasksInsertInsertTask.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("adminTasksInsertInsertTaskInfoMessage", "");
		pageSession.set("adminTasksInsertInsertTaskErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var adminTasksInsertInsertTaskMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(adminTasksInsertInsertTaskMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("adminTasksInsertInsertTaskInfoMessage", message);
					}; break;
				}
			}

			Router.go("admin.tasks", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("adminTasksInsertInsertTaskErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				newId = Tasks.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("admin.tasks", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.AdminTasksInsertInsertTask.helpers({
	"infoMessage": function() {
		return pageSession.get("adminTasksInsertInsertTaskInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("adminTasksInsertInsertTaskErrorMessage");
	}
	
});
