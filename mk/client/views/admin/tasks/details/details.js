var pageSession = new ReactiveDict();

Template.AdminTasksDetails.rendered = function() {
	
};

Template.AdminTasksDetails.events({
	
});

Template.AdminTasksDetails.helpers({
	
});

Template.AdminTasksDetailsDetailsTasks.rendered = function() {
	

	pageSession.set("adminTasksDetailsDetailsTasksInfoMessage", "");
	pageSession.set("adminTasksDetailsDetailsTasksErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.AdminTasksDetailsDetailsTasks.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("adminTasksDetailsDetailsTasksInfoMessage", "");
		pageSession.set("adminTasksDetailsDetailsTasksErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var adminTasksDetailsDetailsTasksMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(adminTasksDetailsDetailsTasksMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("adminTasksDetailsDetailsTasksInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("adminTasksDetailsDetailsTasksErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		Router.go("admin.tasks", {});
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("admin.tasks", {});
	}

	
});

Template.AdminTasksDetailsDetailsTasks.helpers({
	"infoMessage": function() {
		return pageSession.get("adminTasksDetailsDetailsTasksInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("adminTasksDetailsDetailsTasksErrorMessage");
	}
	
});
