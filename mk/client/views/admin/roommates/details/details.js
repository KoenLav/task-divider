var pageSession = new ReactiveDict();

Template.AdminRoommatesDetails.rendered = function() {
	
};

Template.AdminRoommatesDetails.events({
	
});

Template.AdminRoommatesDetails.helpers({
	
});

Template.AdminRoommatesDetailsDetailsRoommates.rendered = function() {
	

	pageSession.set("adminRoommatesDetailsDetailsRoommatesInfoMessage", "");
	pageSession.set("adminRoommatesDetailsDetailsRoommatesErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.AdminRoommatesDetailsDetailsRoommates.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("adminRoommatesDetailsDetailsRoommatesInfoMessage", "");
		pageSession.set("adminRoommatesDetailsDetailsRoommatesErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var adminRoommatesDetailsDetailsRoommatesMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(adminRoommatesDetailsDetailsRoommatesMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("adminRoommatesDetailsDetailsRoommatesInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("adminRoommatesDetailsDetailsRoommatesErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		Router.go("admin.roommates", {});
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("admin.roommates", {});
	}

	
});

Template.AdminRoommatesDetailsDetailsRoommates.helpers({
	"infoMessage": function() {
		return pageSession.get("adminRoommatesDetailsDetailsRoommatesInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("adminRoommatesDetailsDetailsRoommatesErrorMessage");
	}
	
});
