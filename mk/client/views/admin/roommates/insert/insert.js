var pageSession = new ReactiveDict();

Template.AdminRoommatesInsert.rendered = function() {
	
};

Template.AdminRoommatesInsert.events({
	
});

Template.AdminRoommatesInsert.helpers({
	
});

Template.AdminRoommatesInsertInsertRoommate.rendered = function() {
	

	pageSession.set("adminRoommatesInsertInsertRoommateInfoMessage", "");
	pageSession.set("adminRoommatesInsertInsertRoommateErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.AdminRoommatesInsertInsertRoommate.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("adminRoommatesInsertInsertRoommateInfoMessage", "");
		pageSession.set("adminRoommatesInsertInsertRoommateErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var adminRoommatesInsertInsertRoommateMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(adminRoommatesInsertInsertRoommateMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("adminRoommatesInsertInsertRoommateInfoMessage", message);
					}; break;
				}
			}

			Router.go("admin.roommates", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("adminRoommatesInsertInsertRoommateErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				newId = Roommates.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("admin.roommates", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.AdminRoommatesInsertInsertRoommate.helpers({
	"infoMessage": function() {
		return pageSession.get("adminRoommatesInsertInsertRoommateInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("adminRoommatesInsertInsertRoommateErrorMessage");
	}
	
});
