var pageSession = new ReactiveDict();

Template.AdminRoommatesEdit.rendered = function() {
	
};

Template.AdminRoommatesEdit.events({
	
});

Template.AdminRoommatesEdit.helpers({
	
});

Template.AdminRoommatesEditEditRoommate.rendered = function() {
	

	pageSession.set("adminRoommatesEditEditRoommateInfoMessage", "");
	pageSession.set("adminRoommatesEditEditRoommateErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.AdminRoommatesEditEditRoommate.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("adminRoommatesEditEditRoommateInfoMessage", "");
		pageSession.set("adminRoommatesEditEditRoommateErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var adminRoommatesEditEditRoommateMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(adminRoommatesEditEditRoommateMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("adminRoommatesEditEditRoommateInfoMessage", message);
					}; break;
				}
			}

			Router.go("admin.roommates", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("adminRoommatesEditEditRoommateErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				Roommates.update({ _id: t.data.roommate_empty._id }, { $set: values }, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("admin.roommates", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.AdminRoommatesEditEditRoommate.helpers({
	"infoMessage": function() {
		return pageSession.get("adminRoommatesEditEditRoommateInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("adminRoommatesEditEditRoommateErrorMessage");
	}
	
});
