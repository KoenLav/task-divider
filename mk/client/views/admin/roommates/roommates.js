var pageSession = new ReactiveDict();

Template.AdminRoommates.rendered = function() {
	
};

Template.AdminRoommates.events({
	
});

Template.AdminRoommates.helpers({
	
});

var AdminRoommatesViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("AdminRoommatesViewSearchString");
	var sortBy = pageSession.get("AdminRoommatesViewSortBy");
	var sortAscending = pageSession.get("AdminRoommatesViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["name", "email"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var AdminRoommatesViewExport = function(cursor, fileType) {
	var data = AdminRoommatesViewItems(cursor);
	var exportFields = [];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.AdminRoommatesView.rendered = function() {
	pageSession.set("AdminRoommatesViewStyle", "table");
	
};

Template.AdminRoommatesView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("AdminRoommatesViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("AdminRoommatesViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("AdminRoommatesViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("admin.roommates.insert", {});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		AdminRoommatesViewExport(this.roommates, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		AdminRoommatesViewExport(this.roommates, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		AdminRoommatesViewExport(this.roommates, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		AdminRoommatesViewExport(this.roommates, "json");
	}

	
});

Template.AdminRoommatesView.helpers({

	"insertButtonClass": function() {
		return Roommates.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.roommates || this.roommates.count() == 0;
	},
	"isNotEmpty": function() {
		return this.roommates && this.roommates.count() > 0;
	},
	"isNotFound": function() {
		return this.roommates && pageSession.get("AdminRoommatesViewSearchString") && AdminRoommatesViewItems(this.roommates).length == 0;
	},
	"searchString": function() {
		return pageSession.get("AdminRoommatesViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("AdminRoommatesViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("AdminRoommatesViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("AdminRoommatesViewStyle") == "gallery";
	}

	
});


Template.AdminRoommatesViewTable.rendered = function() {
	
};

Template.AdminRoommatesViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("AdminRoommatesViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("AdminRoommatesViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("AdminRoommatesViewSortAscending") || false;
			pageSession.set("AdminRoommatesViewSortAscending", !sortAscending);
		} else {
			pageSession.set("AdminRoommatesViewSortAscending", true);
		}
	}
});

Template.AdminRoommatesViewTable.helpers({
	"tableItems": function() {
		return AdminRoommatesViewItems(this.roommates);
	}
});


Template.AdminRoommatesViewTableItems.rendered = function() {
	
};

Template.AdminRoommatesViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		Router.go("admin.roommates.details", {roommateId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		Roommates.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						Roommates.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("admin.roommates.edit", {roommateId: this._id});
		return false;
	}
});

Template.AdminRoommatesViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return Roommates.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return Roommates.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
