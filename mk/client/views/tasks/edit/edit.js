var pageSession = new ReactiveDict();

Template.TasksEdit.rendered = function() {
	
};

Template.TasksEdit.events({
	
});

Template.TasksEdit.helpers({
	
});

Template.TasksEditEditRoommate.rendered = function() {
	

	pageSession.set("tasksEditEditRoommateInfoMessage", "");
	pageSession.set("tasksEditEditRoommateErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.TasksEditEditRoommate.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("tasksEditEditRoommateInfoMessage", "");
		pageSession.set("tasksEditEditRoommateErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var tasksEditEditRoommateMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(tasksEditEditRoommateMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("tasksEditEditRoommateInfoMessage", message);
					}; break;
				}
			}

			Router.go("tasks", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("tasksEditEditRoommateErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				Tasks.update({ _id: t.data.task_empty._id }, { $set: values }, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("tasks", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.TasksEditEditRoommate.helpers({
	"infoMessage": function() {
		return pageSession.get("tasksEditEditRoommateInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("tasksEditEditRoommateErrorMessage");
	}
	
});
